from enum import Enum
import pickle
# from typing import TypedDict, List
from constants import LENGTH, HERO, LANGUAGE

class Language(Enum):
    EN = 1
    DE = 2
    FR = 3
    PT = 4

class IType(Enum):
    BOOK = 1
    MOVIE = 2
    ARTICLE = 3

# here we define the filterable categories for each item-type and their options
#   e.g. book has Length, Language and Hero
#   the hero can be any animal, human, bear, kid

defaultCategories = {
    IType.BOOK: {
        LENGTH: {
            "options": ["short", "middle", "long", "epic"],
            "question": "How long should your book be?"
        },
        HERO: {
            "options": ["human", "animal", "bear", "kid"],
            "question": "What should the main character be?"
        },
        LANGUAGE: {
            "options": [Language.EN, Language.DE, Language.PT],
            "question": "What language do you want to read in?"
        }
    },
    IType.MOVIE: {
        "forChildren": {
            "options": [True, False],
            "question": "Do you want to watch it with children?"
        }
    }
}

books = {
    {
        "title": "Call of the Wild",
        # tags are tuples (pairs) of values and their respective category
        # the category can also be empty
        "tags": [("dog", HERO), ("short", LENGTH), ("poetic", None), (Language.EN, LANGUAGE)],
        "addedBy": "Julius",
        "readBy": ["Julius", "Hugo"]
    }
}

class Item(object): 
    """ An item is a general instance of something we want to filter for, e.g. a book, article, movie """
    def __init__(self, name, tags = [], description = ""):
        self.type = None
        self.addedBy = None
        self.name = name
        self.tags = tags
        self.description = description
        # self.categories = {}

class Book(Item):
    def __init__(self, name, tags):
        Item.__init__(self, name, tags)
        self.type = IType.BOOK

class Collection():
    def __init__(self, name = ""):
        if name:
            self.collection = pickle.load(open(name, 'rb'))
        else: 
            self.collection = {}
        self.name = name or "test"
        print("collection is initialized with", self.collection)

    # def add_item(self, new_item, user):
    def add_item(self, key, value):
        # TODO handle collision
        self.collection[key] = value
        self.save_to_file()

    def mark_item_as_read_by(self, item_id, user):
        pass

    def generate_possible_categories(self, added_by = "all", exclude_known_items = True):
        pass

    # def load_from_file(self, name):
    #     self.collection = pickle.load(open(name, 'rb'))
    #     print("collection loaded to file!", self.collection)

    def save_to_file(self):
        with open(self.name, 'wb') as collection_file:
            pickle.dump(self.collection, collection_file)
        print("collection saved to file!")

# TESTING 
a = Book("bsfdsf", [1,2,3])
print(a)
print(a.type)

c = Collection()
print(c.collection)
c.add_item("1", "sdfsdf")
c.add_item("4", "ahahah")
print(c.collection)

b = Collection("test")